package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String nacionalidad = "guatemalteco";
        String cui = request.getParameter("InputCUI");
        String nom1 = request.getParameter("InputPrimerNombre");
        String nom2 = request.getParameter("InputSegundoNombre");
        String nom3 = request.getParameter("InputTercerNombre");
        String apellido1 = request.getParameter("InutPrimerApellido");
        String apellido2 = request.getParameter("InputSegundoApellido");
        String sexo = request.getParameter("sexo");

        String fechaNacimiento="";
        String escolaridad = request.getParameter("escolaridad");
        String dptoResidencia="";
        String municipioResidencia="";

        String direResidencia = request.getParameter("InputDireResidencia");
        String pueblo="";
        String comunidadLinguistica="";

        String correo = request.getParameter("InputCorreo");
        String celular = request.getParameter("InputCelular");
        String linea = request.getParameter("linea");

        String confirmatelefono = request.getParameter("InputTelefono");
        String discapacidad="";
        String enfermedadCronica="";
        String afiliacion=request.getParameter("afiliacion");;
        String puestoDpto="";
        String puestoMunicipio="";
        String puestoVacunacion="";

        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");
            out.println("<link href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">");
            out.println("<script src=\"//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js\"></script>");
            out.println("<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>");
            out.println("</head>");
            out.println("<body>");
            out.println("<nav class=\"navbar navbar-light bg-primary\">");
            out.println("<a class=\"navbar-brand\"  style=\"color:white\">Registro de Datos</a>");
            out.println("</nav>");
            out.println("<br><br>");
            
            out.println("<div class=\"container\">");
            out.println("<div class=\"jumbotron jumbotron-fluid\">");
            out.println("<div class=\"container\">");
            out.println("<h1 class=\"display-4\">Registro Exitoso</h1>");
            out.println("<p class=\"lead\">Cuando sea tu momento #Vacúnate</p>");
            out.println("</div>");
            out.println("</div>"); 

            out.println("<div class=\"row\">");
	    out.println("<div class=\"col-md-6\" \"table table-responsive\">");
	    out.println("<div class=\"panel panel-primary\" >");
	    out.println("<div class=\"panel-heading\"><h3 class=\"panel-title\">Datos Registrados</h3></div>");
            out.println("<table>");
            out.println("<tbody>");
            out.println("<tr>");
            out.println("<th>&nbsp;</th>");
            out.println("<td>&nbsp;</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Nacionalidad&nbsp;&nbsp;</th>");
            out.println("<td>"+nacionalidad+"</td>");
            out.println("</tr>");
            
            out.println("<th>C.U.I.:&nbsp;&nbsp;</th>");
            out.println("<td>"+cui+"</td>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Primer Nombre:&nbsp;&nbsp; </th>");
            out.println("<td>"+nom1+"</td>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Segundo Nombre:&nbsp;&nbsp;</th>");
            out.println("<td>"+nom2+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Tercer Nombre:&nbsp;&nbsp;</th>");
            out.println("<td>"+nom3+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Primer Apellido:&nbsp;&nbsp;</th>");
            out.println("<td>"+apellido1+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Segundo Apellido:&nbsp;&nbsp;</th>");
            out.println("<td>"+apellido2+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Sexo:&nbsp;&nbsp;</th>");
            out.println("<td>"+sexo+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Fecha Nacimiento:&nbsp;&nbsp;</th>");
            out.println("<td>"+fechaNacimiento+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Escolaridad:&nbsp;&nbsp;</th>");
            out.println("<td>"+escolaridad+"</t>");
            out.println("</tr>");
           
            out.println("<tr>");
            out.println("<th>Dpto. de Residencia:&nbsp;&nbsp;&nbsp;&nbsp;</th>");
            out.println("<td>"+direResidencia+"</t>");
            out.println("</tr>");

            
            out.println("<tr>");
            out.println("<th>Municipio Residencia:&nbsp;&nbsp;</th>");
            out.println("<td>"+municipioResidencia+"</t>");
            out.println("</tr>");

            out.println("<tr>");
            out.println("<th>Pueblo:&nbsp;&nbsp;</th>");
            out.println("<td>"+pueblo+"</t>");
            out.println("</tr>");
   
            out.println("<tr>");
            out.println("<th>Comunidad Linguistica:&nbsp;&nbsp;</th>");
            out.println("<td>"+comunidadLinguistica+"</t>");
            out.println("</tr>");

            out.println("<tr>");
            out.println("<th>Correo Electróico:&nbsp;&nbsp;</th>");
            out.println("<td>"+correo+"</t>");
            out.println("</tr>");

            
            out.println("<tr>");
            out.println("<th>Teléfono Celular:&nbsp;&nbsp;</th>");
            out.println("<td>"+celular+"</t>");
            out.println("</tr>");
            
            
            out.println("<tr>");
            out.println("<th>Linea:&nbsp;&nbsp;</th>");
            out.println("<td>"+linea+"</t>");
            out.println("</tr>");
            
            
            out.println("<tr>");
            out.println("<th>Confirmar Teléfono:&nbsp;&nbsp;</th>");
            out.println("<td>"+confirmatelefono+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Discapacidad:&nbsp;&nbsp;</th>");
            out.println("<td>"+discapacidad+"</t>");
            out.println("</tr>");
               
            out.println("<tr>");
            out.println("<th>Enfermedad Cronica:&nbsp;&nbsp;</th>");
            out.println("<td>"+enfermedadCronica+"</t>");
            out.println("</tr>");
        
            out.println("<tr>");
            out.println("<th>Afiliación de IGSS:&nbsp;&nbsp;</th>");
            out.println("<td>"+afiliacion+"</t>");
            out.println("</tr>");
                  
            out.println("<tr class=\"btn btn-primary\">");
            out.println("<th>Puesto de Vacunación</th>");
            out.println("<td></t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Departamento:&nbsp;&nbsp;</th>");
            out.println("<td>"+puestoDpto+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>Municipio:&nbsp;&nbsp;</th>");
            out.println("<td>"+puestoMunicipio+"</t>");
            out.println("</tr>");
      
            out.println("<tr>");
            out.println("<th>Puesto de Vacunación:&nbsp;&nbsp;</th>");
            out.println("<td>"+puestoVacunacion+"</t>");
            out.println("</tr>");
            
            out.println("<tr>");
            out.println("<th>&nbsp;</th>");
            out.println("<td>&nbsp;</t>");
            out.println("</tr>");
    
         
     
            
            out.println("</tbody>");
            out.println("</table>");
            //out.println("<button type=\"button\" class=\"btn btn-primary btn-lg btn-block\">Guardar</button>");
            out.println("<br>");
            out.println("<a href=\"index.jsp\"><input type=\"button\" value=\"Regresar\" class=\"btn btn-secondary btn-lg btn-block\" href=\"http://localhost:8080/WebApp/\"></button>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("<br><br>");
            out.println("<br><br>");
            out.println("</body>");
            out.println("</html>");
            
            
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
