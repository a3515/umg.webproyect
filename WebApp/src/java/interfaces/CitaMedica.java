package interfaces;

public interface CitaMedica {

    public void imprimirCita();

    public void imprimirConstancia(String descripcion);

}
